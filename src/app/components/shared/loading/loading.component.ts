import { Component, OnInit } from '@angular/core';
import { faSync, faSpinner } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html'
})
export class LoadingComponent implements OnInit {

  faSync = faSync;
  faSpinner = faSpinner;

  constructor() { }

  ngOnInit() {
  }

}
